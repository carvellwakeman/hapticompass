#include <Arduino.h>
#include <bluefruit.h>
#include <xiaobattery.h>

#define BUZZ_PIN D8
#define BTN_PIN D10

// Settings
#define BLE_ADV_FAST_TIMEOUT 30
#define BLE_ADV_TIMEOUT 60

Xiao battery;
BLEDis bledis;

// BLE Service
BLEUart bleuart; // UART comms
BLEService batteryService = BLEService(UUID16_SVC_BATTERY); // BLE battery service

// BLE Battery Level Characteristic
BLECharacteristic batteryLevelChar = BLECharacteristic(UUID16_CHR_BATTERY_LEVEL);

uint8_t buzz = 0;
uint8_t battPct = 0;

void setup()
{
  // GPIO
  pinMode(BUZZ_PIN, OUTPUT);
  pinMode(BTN_PIN, INPUT_PULLUP);
  pinMode(LED_RED, OUTPUT);
  pinMode(LED_GREEN, OUTPUT);
  pinMode(LED_BLUE, OUTPUT);

  // Initial buzz on boot
  buzz = 0;
  analogWrite(BUZZ_PIN, 0);

  // Debug serial
  Serial.begin(9600);
  while ( !Serial ) yield();
  Serial.println("Started");

  // XIAO sleep wakeup
  uint32_t pinMap = g_ADigitalPinMap[BTN_PIN];
  nrf_gpio_cfg_sense_input(pinMap, NRF_GPIO_PIN_PULLUP, NRF_GPIO_PIN_SENSE_LOW);

  // Start bluetooth
  Bluefruit.begin();
  Bluefruit.setTxPower(4);
  Bluefruit.Periph.setConnectCallback(connect_callback);
  Bluefruit.Periph.setDisconnectCallback(disconnect_callback);
  Bluefruit.setName("Hapticompass");

  // Configure and Start Device Information Service
  bledis.setManufacturer("Seeed Studio");
  bledis.setModel("XIAO nRF52840");
  bledis.begin();

  /* Set connection interval (min, max) to your perferred value.
   * Note: It is already set by BLEHidAdafruit::begin() to 11.25ms - 15ms
   * min = 9*1.25=11.25 ms, max = 12*1.25= 15 ms 
   */
  /* Bluefruit.Periph.setConnInterval(9, 12); */

  // Configure and start BLE UART service
  bleuart.begin();

  // Configure and start BLE Battery service and characteristic
  batteryService.begin();
  batteryLevelChar.setProperties(CHR_PROPS_INDICATE);
  batteryLevelChar.setPermission(SECMODE_OPEN, SECMODE_NO_ACCESS);
  batteryLevelChar.setFixedLen(1);
  batteryLevelChar.setCccdWriteCallback(cccd_callback);
  batteryLevelChar.begin();
  batteryLevelChar.write(&battPct, sizeof(battPct)); // set initial value for this characteristic

  // Set up and start advertising
  startAdv();
}


void startAdv(void)
{  
  // Advertising packet
  Bluefruit.Advertising.addFlags(BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE);
  Bluefruit.Advertising.addTxPower();
  Bluefruit.Advertising.addAppearance(BLE_APPEARANCE_GENERIC_WATCH);

  // Include bleuart 128-bit uuid
  Bluefruit.Advertising.addService(bleuart);
  Bluefruit.Advertising.addService(batteryService);

  // There is enough room for the dev name in the advertising packet
  Bluefruit.Advertising.addName();
  
  /* Start Advertising
   * - Enable auto advertising if disconnected
   * - Interval:  fast mode = 20 ms, slow mode = 152.5 ms
   * - Timeout for fast mode is 30 seconds
   * - Start(timeout) with timeout = 0 will advertise forever (until connected)
   * 
   * For recommended advertising interval
   * https://developer.apple.com/library/content/qa/qa1931/_index.html   
   */
  Bluefruit.Advertising.setStopCallback(stopAdv_callback);
  Bluefruit.Advertising.restartOnDisconnect(true);
  Bluefruit.Advertising.setInterval(32, 244);    // in unit of 0.625 ms
  Bluefruit.Advertising.setFastTimeout(BLE_ADV_FAST_TIMEOUT);      // number of seconds in fast mode
  Bluefruit.Advertising.start(BLE_ADV_TIMEOUT);               // Stop advertising after n seconds
}


/**
 * Callback invoked when advertising stops
 */
void stopAdv_callback() {
  Serial.println("Advertising timeout, powering off");
  digitalWrite(LED_RED, LOW);
  digitalWrite(LED_GREEN, HIGH);
  digitalWrite(LED_BLUE, HIGH);
  delay(5000);
  digitalWrite(LED_RED, HIGH);
  delay(100);
  sd_power_system_off();
}

/**
 * Callback invoked when central connects
 * @param conn_handle connection where this event happens
 */
void connect_callback(uint16_t conn_handle)
{
  // Get the reference to current connection
  BLEConnection* connection = Bluefruit.Connection(conn_handle);

  char central_name[32] = { 0 };
  connection->getPeerName(central_name, sizeof(central_name));

  Serial.print("Connected to ");
  Serial.println(central_name);

  digitalWrite(LED_GREEN, LOW);
  delay(3000);
  digitalWrite(LED_GREEN, HIGH);
}

/**
 * Callback invoked when a connection is dropped
 * @param conn_handle connection where this event happens
 * @param reason is a BLE_HCI_STATUS_CODE which can be found in ble_hci.h
 */
void disconnect_callback(uint16_t conn_handle, uint8_t reason)
{
  (void) conn_handle;
  (void) reason;

  Serial.println();
  Serial.print("Disconnected, reason = 0x"); Serial.println(reason, HEX);

  buzz = 0; // Turn off vibrate when disconnected
}


void cccd_callback(uint16_t conn_hdl,
  BLECharacteristic* chr, uint16_t cccd_value) {
    // Display the raw request packet
    Serial.print("[INFO] CCCD Updated: ");

    // Serial.printBuffer(request->data, request->len);
    Serial.print(cccd_value);
    Serial.println("");

    // Check the characteristic this CCCD update is associated with in case
    // this handler is used for multiple CCCD records.
    if (chr->uuid == batteryLevelChar.uuid) {
        if (chr->indicateEnabled(conn_hdl)) {
            Serial.println("[INFO] Battery level 'Indicate' enabled");
        } else {
            Serial.println("[INFO] Battery level 'Indicate' disabled");
        }
    }
}

void loop()
{
  analogWrite(BUZZ_PIN, buzz);

  if (battery.IsChargingBattery()) {
   
  }

   if (Bluefruit.connected()) {
    // Note: We use .indicate instead of .write!
    // If it is connected but CCCD is not enabled
    // The characteristic's value is still updated although indicate is not sent
    if (batteryLevelChar.indicate(&battPct, sizeof(battPct))) {
      Serial.print("[INFO] Updated Battery level: ");
      Serial.print(battPct);
      Serial.println("%");
    } else {
      Serial.println("[ERROR] Battery level Indicate not set in the CCCD or not connected");
    }
  }

  if (digitalRead(BTN_PIN) == LOW) {
    Serial.println("Button is pressed");
  }
  
  // Serial.println(battery.IsChargingBattery());
  // Serial.println(battery.GetBatteryVoltage());

  // Forward from BLEUART to HW Serial
  while ( bleuart.available() )
  {
    uint8_t ch;
    ch = (uint8_t) bleuart.read();
    Serial.write(ch);
    buzz = ch;
  }
}

// void sendResponse(char const *response) {
//     Serial.printf("Send Response: %s\n", response);
//     bleuart.write(response, strlen(response)*sizeof(char));
// }
