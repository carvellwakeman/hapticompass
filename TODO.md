

# firmware
- [X] BT advertising stops after configured timeout
- [x] Deep sleep after adv timeout
- [x] Deep sleep wakeup on button press
- [] Add settings loaded from flash on started (configured timeout, initial vibrationn, etc)
- [] Add BT battery service
- [] Add BT OTA service
- [] Name the services?

# hardware
- [x] Add reset button
- [] Design prototype "enclosure" to print (big battery first?)

# app
- [] Initial app that displays bluetooth devices
- [] Select BT device and connect to it, show connected devices.
- [] Filter by name or some other adv criteria "Hapticompass"
- [] Remember last paired device
- [] Some sort of background service that can be turned off.
- [] Write BT payload with button press
- [] Find absolute orientation
- [] Write BT payload when within 5 degrees of north
- [] Experiment with vibration patterns
- [] Display battery life